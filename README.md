# Scripts, tools, and resources for the 5 countries project servers

All server related Ansible scripts expects to be run against a clean Debian Stretch x64
installation.

## Directroies

 * `backup-cfg`: Scripts for server to backup router configurations.
 * `ansible`: Ansible playbook to install and configure all ansible controlled components of the network.
