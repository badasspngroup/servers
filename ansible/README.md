# 5 Contries project ansible playbook

This is the Ansible playbook for setting up most servers and certain configuration aspects of the vMX routers configuration.

## The play book

The playbook maps the tasks in the roles according to the host function in the
network. The playbook has separate tasks for

 * Seting up time zone and install ntp and rsyslog on Debian systems
 * Installing snmp and configuring SNMP for LibreNMS on Debian systems
 * Configuring the vMX routers (SNMP, Syslog, NTP, Name server, Router Advertisment)
 * Installing the Elastic stack, Kibana, and libreNMS on the monitoring server.
 * Install and configure the web servers

## The inventory

The inventory is were all hosts under Ansible control are defined by host
name:

### Hosts

* Development
 * deploy.group3.dk
* Monitoring servers
 * monitor.group3.dk
* Name Servers
* dns.group3.dk
* Web servers
* web.group3.dk
* Routers
* vmx-copenhagen.group3.dk
* vmx-odense.group3.dk
* vmx-aalborg.group3.dk
* Firewalls
 * paloalto.group3.dk

### Groupings

* SNMP
 * Monitoring servers
 * Name Servers
 * Web servers
 * Development

* LibreNMS hosts
 * Name Servers
 * Web servers
 * Development
 * Routers

* Debian hosts
 * Name Servers
 * Web servers
 * Development
 * Monitoring servers

## Usage

To run the complete playbook on all hosts:

    ansible-playbook -i inventory/hosts playbook.yml

**Limiting to certain hosts ands tags**

Limit the playbook to certain hosts or groups by adding them after the `-l` parameter:

    ansible-playbook -i inventory/hosts playbook.yml -l webservers

The above only runs the task that are applicable to the `webservers` group. You
can also limit the playbook to running task with certain tags associated to them:

    ansible-playbook -i inventory/hosts playbook.yml --tags "snmp,junos"

The above command only runs the tasks associated with the tags `snmp`, and `junos`.

## Files & folders

  * *inventory*: The inventory where host, groups and variables for both are
    defined.
      * *group_vars*: Variables by group.
          * *monitor*: Variables specific to the monitoring server.
          * *snmp*: Variables specific to SNMP configuration.
          * *all.yml*: Variables for all hosts in the inventory.
      * *host_vars*: Variables by host.
          * *vmx-\**: Variables specific to the vMX routers.
          * *web.group3.dk*: Variables specific to the web server.
      * *hosts*: The inventory file.
  * *roles/*: Roles used in the playbook.
      * *ansible-apache2-debian*: Role to install an apache2 web server on Debian.
      * *ansible-integrated-elks*: Role to install the Elastic Stack on Debian.
      * *ansible-junos-nameserver*: Role to set the name server in the vMXs.
      * *ansible-junos-ntp*: Role to set the NTP server address in the vMXs.
      * *ansible-junos-ra*: Role to set up router advertisment in the vMXs.
      * *ansible-junos-snmp*: Role to set up SNMP configuration in the vMXs.
      * *ansible-junos-syslog*: Role to set up logging for the Elastic Stack in the vMXs.
      * *ansible-librenms*: Role to install LibreNMS on Debian.
      * *ansible-librenms-addhosts6*:  Role to add an IPv6 host to LibreNMS.
      * *ntp-debian*: Role to set the NTP server address on Debian.
      * *rsyslog-debian*: Role to set up logging for the Elastic Stack on Debian.
      * *snmpd-debian*: Role to set up SNMP configuration on Debian.
      * *timezone-debian*: Role set the time zonbe on Debain.
  * *diffs*: Files with configuration differences used during vMX configuration.
  * *logs*: Log files from vMX configuration.
  * *rendered*: Rendered templates of configuration files used during vMX configuration.
  * *playbook.yml*: Main Ansible playbook.
