# 5 Countries project Ansible roles.

* *ansible-apache2-debian*: Role to install an apache2 web server on Debian.
* *ansible-integrated-elks*: Role to install the Elastic Stack on Debian.
* *ansible-junos-nameserver*: Role to set the name server in the vMXs.
* *ansible-junos-ntp*: Role to set the NTP server address in the vMXs.
* *ansible-junos-ra*: Role to set up router advertisment in the vMXs.
* *ansible-junos-snmp*: Role to set up SNMP configuration in the vMXs.
* *ansible-junos-syslog*: Role to set up logging for the Elastic Stack in the vMXs.
* *ansible-librenms*: Role to install LibreNMS on Debian.
* *ansible-librenms-addhosts6*:  Role to add an IPv6 host to LibreNMS.
* *ntp-debian*: Role to set the NTP server address on Debian.
* *rsyslog-debian*: Role to set up logging for the Elastic Stack on Debian.
* *snmpd-debian*: Role to set up SNMP configuration on Debian.
* *timezone-debian*: Role set the time zonbe on Debain.
