# Inventory

Host are defined and grouped in `hosts`, the directories `group_vars` and
`host_vars` contains variable definitions for specific groups and hosts.

## Hosts and groups

### Hosts

* Development
 * deploy.group3.dk
* Monitoring servers
 * monitor.group3.dk
* Name Servers
* dns.group3.dk
* Web servers
* web.group3.dk
* Routers
* vmx-copenhagen.group3.dk
* vmx-odense.group3.dk
* vmx-aalborg.group3.dk
* Firewalls
 * paloalto.group3.dk

### Groups

* SNMP
 * Monitoring servers
 * Name Servers
 * Web servers
 * Development

* LibreNMS hosts
 * Name Servers
 * Web servers
 * Development
 * Routers

* Debian hosts
 * Name Servers
 * Web servers
 * Development
 * Monitoring servers

## Group variables

 * *vmx\**: Variables for configuring router advertisment on the vMXs
 * *web.group3.dk*: Variables for the web server (port, domain, repository to clone site from).

## Host variables

The vMXs have additional host variables needed by the Juniper.junos used to configure them:

 * *ansible_connection=local*: Run the ansible Python scripts on the machine local machine.
 * *local ansible_become_pass=xxxx*: Set the password to become root on the vMXs
