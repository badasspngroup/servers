#!/bin/bash

# Remote backup user. Set up public key authentication for this script.
USER="backup"
# Host to back up.
DEVICES="vmx-cph vmx-odense vmx-aalborg"

# Path to the configurations files in the devices.
FILE="/config/*"
# The path in the repository for adding the configuration files.
GIT_DIR="configs/net"
# Git repository URL.
GIT="deadbok@bitbucket.org:badasspngroup/configs.git"

# Find mktemp
TMP_DIR="$(mktemp -d)"
if [ -z "$TMP_DIR" ] ; then
        echo "E: no temp dir created" >&2
        exit 1
fi

# Find git
GIT_BIN="$(which git)"
if [ ! -x "$GIT_BIN" ] ; then
        echo "E: no git binary" >&2
        exit 1
fi

# Find scp
SCP_BIN="$(which scp)"
if [ ! -x "$SCP_BIN" ] ; then
        echo "E: no scp binary" >&2
        exit 1
fi

# Find gunzip
GUNZIP_BIN="$(which gunzip)"
if [ ! -x "$GUNZIP_BIN" ] ; then
        echo "E: no gunzip binary" >&2
        exit 1
fi

# Change into the temporary directory
cd $TMP_DIR
# Clone the current repository.
$GIT_BIN clone $GIT
# Cd into the right folder.
cd $GIT_DIR

# For every device to backup.
for device in $DEVICES; do
        # Make sure the directory is there.
        mkdir -p $device
        # Go in there.
        cd $device

        # Fetch the configuration files from the device.
        CONN="${USER}@${device}"
        $SCP_BIN ${CONN}:${FILE} .

        # Unzip the configuration files.
        $GUNZIP_BIN -f *.gz

        # Commit the files to the repository.
        $GIT_BIN add -A .
        $GIT_BIN commit -m "${device}: configuration change" \
                -m "A configuration change was detected" \
                --author="cron <cron@example.com>"
        cd ..
done

# Push all changes to the remote repository.
$GIT_BIN push -f

# Clean up files.
rm -rf $TMP_DIR
