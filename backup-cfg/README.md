# Configuration backup servers

Scripts to backup configuration files from vital network components by pushing
regular snapshots to a private git repository.

[Blog post on the subject](http://ral-arturo.org/2017/06/23/router-git-backup.html)

## The backup controller

This is a Debian installation setup like this:

 * If local DNS is not available add the management IP of the Juniper devices
 in the host file.
 * Public key added to the BitBucket repository to give the machine access
 without a password.
 * Add the scripts to the cron jobs with the timing you want.

## The juniper devices

Set up a user called `backup` for the controller to fetch the configuration
file. Add the controllers public key to avoid passwords.

    set system login class git permissions maintenance
    set system login class git allow-commands scp.*
    set system login user backup uid 1001
    set system login user backup class git
    set system login user backup authentication ssh-rsa "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCq/OhgqeCgvVyWTIE7iOMy92pg66Eu7NkR609R9gdFvBQaMnBp5ywMQuve1yhb4lM8WkpydUSkpUFdVZJpBKjk1Z0aOymvRoqX8s7tczmdxVbjl6hRdR1TV2HU2hQzkFrQevOFBsxFQM08iHj/H7XERcSxLHv8N1LHQs+nRP4wDkCuMsS1VvDrQ5vpaUs1hOAS3JOJw0AtC0bUvWUbflsgJiMaNXROESz9/chlbo+KeuXPRZYIYtrL2SMN814uZmc3X9nRov3/0Bu8xot2zig3iu6ocpdOnlYjS/sInv6x03T/IMl2CY/tls7k9b63ZyGh1GANuwB6EKtrjonQsapF group3@dvmx-bck"
